#Justin Henn
#Project 4

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

#functions for finding Eout and variance

def func1(x):
    return np.mean((train.a*test.X+train.b-(test.X)**2)**2)
def func2(x):
    return np.mean((train.a * test.X + train.b - train.a_mean * test.X - train.b_mean)**2)


#Creating set to find slope and intercept

N=1000;
np.random.seed(1221);
train = pd.DataFrame(np.random.uniform(-1, 1, N), columns=['x1'])
train = train.assign(x2 = np.random.uniform(-1, 1, N))
train = train.assign(y1 = train.x1*train.x1)
train = train.assign(y2 = train.x2*train.x2)
train = train.assign(a = (train.y1-train.y2)/(train.x1-train.x2))
train = train.assign(b = train.a*(-train.x1)+train.y1)

#finding slope and intercept

train = train.assign(a_mean = np.mean(train.a))
train = train.assign(b_mean = np.mean(train.b))

#creating test set

test = pd.DataFrame(np.random.uniform(-1, 1, 10000), columns=['X'])

#finding Eout, bias, and variance on test set

test = test.assign(Eout = np.apply_along_axis(func1,1, test))
print ("Expected Eout", end= " ")
print (np.mean(test.Eout))
print("Bias", end= " ")
print(np.mean((train.a_mean*test.X+train.b_mean-test.X**2)**2))

test = test.assign(varx = np.apply_along_axis(func2, 1, test))
print("Variance", end=" ")
print(np.mean(test.varx))

#finding y points for x and gbar(x)

train = train.assign(y_testing = np.mean(train.a)*(train.x1)- np.mean(train.b))


axes = plt.gca()
x = np.linspace(-1,1)
plt.plot(x, x*x, '--',label = 'x^2')
plt.plot(train.x1 ,train.y_testing, '--', color = 'black', label = "gbarx")
plt.legend()
plt.show()
